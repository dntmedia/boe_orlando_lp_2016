<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     
      <title>Bank of England Mortgage Orlando</title>
      <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="css/materialize.css">
      <link rel="stylesheet" href="css/main.css">
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
      <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
      <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
      <link rel="manifest" href="/manifest.json">
      <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
      <meta name="theme-color" content="#ffffff">
    </head>

    <body>
      <nav>
        <div class="nav-wrapper grey lighten-4">
          <a href="#" class="brand-logo center"><img src="img/boe_logo1.png"  class="responsive-img" alt="Bank of England Mortgage"></a>
          <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
          <ul class="left hide-on-med-and-down">
            <li><a href="telto:4073337395"><i class="material-icons left">contact_phone</i>Call Us</a></li>
            <li><a href="mailto:orlandoinfo@boemortgage.com"><i class="material-icons left">email</i>Email Us</a></li>
          </ul>
          <ul class="right hide-on-med-and-down">
            <li><a href="https://2613393989.mortgage-application.net/WebApp/Start.aspx" target="_blank"><i class="material-icons left">description</i>Apply Now</a></li>
            <li><a href="https://www.boeorlando.com/"><i class="material-icons left">web</i>BOEOrlando.com</a></li>
          </ul>
          <ul class="side-nav" id="mobile-demo">
            <li><a href="telto:4073337395"><i class="material-icons left">contact_phone</i>Call Us</a></li>
            <li><a href="mailto:orlandoinfo@boemortgage.com"><i class="material-icons left">email</i>Email Us</a></li>
            <li><a href="https://2613393989.mortgage-application.net/WebApp/Start.aspx" target="_blank"><i class="material-icons left">description</i>Apply Now</a></li>
            <li><a href="https://www.boeorlando.com/"><i class="material-icons left">web</i>BOEOrlando.com</a></li>
          </ul>
        </div>
      </nav>
        <div class="row">
          <div class="col m3">
            <div class="sidebar-img" style="margin: 0px">
              <img src="img/time_running_out.jpg" class="responsive-img" alt="Why now? Why Bank of England Mortgage?">
            </div>
            <div class="sidebar-img-mobile">
              <img src="img/time_running_out_mobile.jpg" class="responsive-img" alt="Why now? Why Bank of England Mortgage?">
            </div>
          </div>
          <div class="col m7">
            <h1 style="text-align: center">Why now? Why Bank of England Mortgage?</h1>
            <p class="flow-text">With mortgage rates near all-time lows, it has never been cheaper to own a home.  The average buyer can save hundreds of dollars per month if they could purchase the same home they were renting right now in Central Florida.  The sooner you purchase a home, the sooner you can start creating wealth for your future and your retirement as your equity grows.</p>
            <p class="flow-text">Bank of England Mortgage is a family owned bank from England, Arkansas leveraged with the tools to help families become home owners.  Let our family help your family start building the American dream.</p>

            <h3>Need help with the down payment and/or closing costs?</h3>
            <p class="flow-text">Bank of England Mortgage is proud to partner with the Florida Housing Finance Corporation to help low-to-moderate income families purchase their first home.  This program allows qualifying families to receive up to $15,000.00 in select counties here in central Florida, and up to $7,500.00 for all other counties.   We are also offering 100% financing options.  Contact us today to discuss your options.</p>

            <h3>Waiting for a lease to expire?</h3>
            <p class="flow-text">Working with the right team is everything.  With our team of mortgage professionals and when working with the right realtor, we can time the transition perfectly, making it more affordable to leave a lease early.  Call us today to discuss your challenges, and we will look at the available options to make sure you don’t miss out purchasing your dream home.</p>

            <h3>Credit Problems?</h3>
            <p class="flow-text">There are a lot of misunderstandings about credit and what it takes to buy a home today.  Our team at Bank of England Mortgage are here to help guide you through the process.  We have helped several families repair and fix their credit to achieve their dream of home ownership.  Good, bad or ugly, give us an opportunity to coach you and together we will make the dream happen.</p>

            <h3>Market Fear?</h3>
            <p class="flow-text">Watching the rise and fall and rise again of the real estate market here in central Florida over the last decade, we have seen it all.  No one knows when a peak or trough will occur, but in the end, it boils down to payment.  Rates are near the lowest they have ever been, which translates into the best payments all time.  Call us today to calculate how much we can save you by buying a home.</p>     

            <h3>Contact Us</h3>
            <div class="col m3 s6">
              <a class="waves-effect waves-light btn-large btn-custom" href="telto:4073337395"><i class="material-icons left">contact_phone</i>Call Us</a>
            </div>
            <div class="col m3 s6">
              <a class="waves-effect waves-light btn-large btn-custom" href="mailto:orlandoinfo@boemortgage.com"><i class="material-icons left">email</i>Email Us</a>
            </div>
            <div class="col m3 s6">
              <a class="waves-effect waves-light btn-large btn-custom" href="https://2613393989.mortgage-application.net/WebApp/Start.aspx" target="_blank"><i class="material-icons left">description</i>Apply</a>
            </div>
            
            <div class="col m3 s6">
              <a class="waves-effect waves-light btn-large btn-custom" href="https://www.boeorlando.com/"><i class="material-icons left">web</i>Website</a>
            </div> 

          </div>
          <div class="col m2">
            <h4>Customer Testimonials</h4>
            
            <blockquote>
            <cite>Brenda Morley says..</cite><br>
            The team at Bank of England Mortgage has gone above and beyond my expectations. They were with me every step of the way throughout my purchase process. They did a great job of educating me and holding my hand from start to finish, which i thought was very important to me. I will definitely be giving out their name to friends and family members who are looking to purchase homes in the future! GREAT JOB!</blockquote>
            

            <blockquote>
            <cite>Jason Cox says..</cite><br>
            We reached out to several lenders but it was Travis Kelly with Bank of England Mortgage that was the most knowledgeable, organized, and motivated. As a first time home buyer, I was apprehensive on how to tackle this monumental task. Travis walked us through the entire process, provided a list of what needed to be done, and worked with us to punch through each task. Most importantly, his integrity and honesty is second to none. The work Travis did for us literally changed our lives forever. Our family will be Bank of England customers for life. If any of our friends and family are looking to purchase a home, they will be talking to Travis Kelly and his group first.</blockquote>
            
            <blockquote>
              <cite>Andrea Malmaa says..</cite><br>
              As a realtor with RE/MAX Town Centre, I have over 20 years of experience doing real estate transactions in the US and abroad. I have been working with Jim for over 5 years now. His excellent communication skills and personalization set him apart from nearly every other mortgage professional that I have worked with in the past. He has a vast knowledge of his bank’s products and has been able to use that to his advantage to convert some of the harder purchase transactions that we worked on together. Nearly every deal we have done together has closed quickly and I would highly recommend Jim to anyone looking to secure financing for a new home purchase.
            </blockquote>
          </div>
        </div>        
      </div>
        <footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col l12 s12">
                <p class="white-text">© 2016 Copyright Bank of England Mortgage. All rights reserved.</p>
                <p class="grey-text text-lighten-4">Bank of England Mortgage has tried to provide accurate and timely information; however, the content of this site may not be accurate, complete or current and may include technical inaccuracies or typographical errors. From time to time changes may be made to the content of this site without notice. Bank of England Mortgage may change the products, services, and any other information described on this site at any time. The information published on this site is provided as a convenience to visitors and is for informational purposes only. You should verify all information before relying on it and decisions based on information contained in our site are your sole responsibility. If you are an individual with disabilities who needs accommodation, or you are having difficulty using our website to apply for a loan, please contact us at (407) 333-7395. This contact information is for accommodation requests only.</p>

                <p class="grey-text text-lighten-4"><a href="https://www.boemortgage.com/" target="_blank">Bank of England Mortgage</a> is a division of <a href="https://www.bankofengland-ar.com/" target="_blank">Bank of England</a> and is not affiliated with any government agency. NMLS 418481. <a href="https://research.fdic.gov/bankfind/detail.html?bank=13303&name=Bank+of+England&tabId=3&searchName=Bank+of+England" target="_blank">Member FDIC</a>. <img src="img/littlehouse.png" class="responsive-img" alt="The Bank of England Mortgage is an Equal Housing LenderEqual Housing Lender."></p>
              </div>
            </div>
          </div>
        </footer>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <!-- Compiled and minified JavaScript -->
      <script src="js/materialize.js"></script>
      <script src="js/main.js"></script>
    </body>
  </html>